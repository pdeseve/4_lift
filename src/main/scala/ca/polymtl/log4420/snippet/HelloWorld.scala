package ca.polymtl.log4420.snippet

import net.liftweb._
import http.DispatchSnippet
import util.Helpers._


object HelloWorld extends DispatchSnippet
{
  def dispatch = { case "render" => render }

  def render =
  {
    ".first-name *" #> "Guillaume" &
    ".last-name *" #> "Massé"
  }
}
