package ca.polymtl.log4420
package snippet

// Lift
import net.liftweb._
import sitemap.Loc.Template
import sitemap.{*, Menu}
import http._
import util.Helpers._

import xml.NodeSeq

class Cheminement( cheminement: model.Cheminement )
{
  def render: NodeSeq => NodeSeq =
  {
    ".addresse [href]" #> S.request.map( _.uri ) &
    ".addresse2 [href]" #> ( Cheminement.comite.calcHref( cheminement ) ) &
    ".session *" #> cheminement.sessions.map(
      session => Session( session, cheminement )
    ) // &
    // ".ajouter-session" #> ...
    // ".ajouter-cours" #> ...
    // etc
  }
}

object Cheminement
{
  // permet de convertir un url dans un cheminement
  // url: cheminement / comite / titre cheminement / annee debut
  // https://github.com/MasseGuillaume/SEO-Friendly-Menu-Params
  val menuComite = Menu.params[ model.Cheminement ](
    "CheminementComite",                                                                  // name
    "Cheminement Comité",                                                                 // link text
    model.Cheminement.findByComite _,
    model.Cheminement.pathByComite _                                                               // model => List[String]
  ) / "Cheminement" / * / * / * >> template                                               // url: Cheminement / Logiciel / Multimedia / 2009

  lazy val comite = menuComite.toLoc                                                      // loc to add in sitemap

  val menuCheminement = Menu.param[ model.Cheminement ](                                // ibid
    "Cheminement",
    "Cheminement",
    model.Cheminement.findById _,
    model.Cheminement.pathById _
  ) / "Cheminement" / * >> template                                                       // url: Cheminement / cheminement.hashcode

  lazy val cheminement = menuCheminement.toLoc

  // load template: webapp / Cheminement.html
  lazy val template = Template( () => Templates( "Cheminement" :: Nil ) openOr <b>template not found</b> )
}